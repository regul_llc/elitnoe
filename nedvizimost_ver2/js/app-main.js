$(function() {
	$(window).on('resize', function(event) {
		var windowSize = document.documentElement.clientWidth;
		var $parent = $('.page-search-results .lots-item');

		if (windowSize <= 320) {
			$parent.each(function() {
				var $parentRight = $(this).children('.__right');
				var $parentRows = $(this).find('.__row.__price, .__row.__link');
				$parentRight.append($parentRows);
			});
			
		} else {
			$parent.each(function() {
				var $parentLeft = $(this).children('.__left');
				var $parentRows = $(this).find('.__row.__price, .__row.__link');
				$parentLeft.append($parentRows);
			});
		}
	});
	$(window).trigger('resize');




	// mask phone
	$('[data-role="js-phone"]').mask('+7 (000) 000 00-00');




	// open modal popup
	/*$('[data-role="js-open-modal"]').on('click', function(event){
		event.preventDefault();

		var $popup = $($(this).data('target'));
		if ($popup.length > 0) {
		}
	});*/

	// popup-enter position
	var positionX = $('#header').offset().left + $('#header').width();
	$('.popup-enter').css('left', positionX + 'px');




	// data-role="js-dropdown"
	$('[data-role="js-dropdown"] .jq-selectbox__select').on('click', function(event) {
		$(this).closest('[data-role="js-dropdown"]').toggleClass('opened');
	});
	$('[data-role="js-dropdown"]').on('click', function(event) {
		event.stopPropagation();
	});
	$('[data-role="js-dropdown"] .jq-selectbox__dropdown li a').on('click', function(event) {
		$('[data-role="js-dropdown"].opened').removeClass('opened');
	});
	$(document).on('click', function(event) {
		// event.preventDefault();

		$('[data-role="js-dropdown"].opened').removeClass('opened');
	});




	$('[data-role="radio-switch"]').buttonset();




	// input file
	$('.__file-upload .file-button .image').on('click', function(event){
		var $parent = $(this).closest('.file-button');
		$parent.find('input[type="file"]').trigger('click');
		console.log($parent.find('input[type="file"]'));
	});




	// input css hover
	$('.form-group.__input [class*="control"]').on('focus', function(event) {
		$(this).closest('.form-group.__input').addClass('__focused');
	});
	$('.form-group.__input [class*="control"]').on('blur', function(event) {
		$(this).closest('.form-group.__input').removeClass('__focused');
	});

	$('.form-group.__input-2-values .__input-value-group [class*="control"]').on('focus', function(event) {
		$(this).closest('.__input-value-group').addClass('__focused');
	});
	$('.form-group.__input-2-values .__input-value-group [class*="control"]').on('blur', function(event) {
		$(this).closest('.__input-value-group').removeClass('__focused');
	});




	// slide card description
	$('[data-role="js-card-toggle"]').on('click', function(event) {
		event.preventDefault();

		var $parent = $(this).closest('.lots-item');
		$parent.find('[data-role="js-card-text"]').slideToggle(300);

		var text = $(this).text();
		if (text === 'Подробное описание') {
			text = 'Скрыть описание';
		} else if (text === 'Скрыть описание') {
			text = 'Подробное описание';
		}
		$(this).text(text);
	});




	// show agents contacts
	$('[data-role="js-agent-contacts-link"]').on('click', function(event) {
		event.preventDefault();

		var $parent = $(this).closest('[data-role="js-agent-contacts"]');
		var $contacts = $parent.find('[data-role="js-agent-contacts-main"]');
		$contacts.slideToggle(300);

		var $link = $parent.find('[data-role="js-agent-contacts-link"]');
		var text = $link.text();
		if (text === 'Скрыть контакты агента') {
			$link.text('Показать контакты агента');
		} else {
			$link.text('Скрыть контакты агента');
		}

		$parent.toggleClass('__is-open');
	});




	// detailed-info state
	$('[data-role="js-detailed-state-1"]').on('click', function(event) {
		event.preventDefault();

		var $parent = $(this).closest('.detailed-info');
		$parent.addClass('__state-1');
		$parent.removeClass('__state-2 __state-3 __state-4');
	});
	$('[data-role="js-detailed-state-2"]').on('click', function(event) {
		event.preventDefault();

		var $parent = $(this).closest('.detailed-info');
		$parent.addClass('__state-2');
		$parent.removeClass('__state-1 __state-3 __state-4');
	});
	$('[data-role="js-detailed-state-3"]').on('click', function(event) {
		event.preventDefault();

		var $parent = $(this).closest('.detailed-info');
		$parent.addClass('__state-3');
		$parent.removeClass('__state-1 __state-2 __state-4');
	});
	$('[data-role="js-detailed-state-4"]').on('click', function(event) {
		event.preventDefault();

		var $parent = $(this).closest('.detailed-info');
		$parent.addClass('__state-4');
		$parent.removeClass('__state-1 __state-2 __state-3');
	});




	// show lots table
	$('[data-role="js-show-lots-table"]').on('click', function(event) {
		$('body').addClass('__show-lots-table');
	});
	$('[data-role="js-show-lots-blocks"]').on('click', function(event) {
		$('body').removeClass('__show-lots-table');
	});




	$('[data-role="js-button-group"] button').on('click', function(event) {
		$(this).siblings('button').removeClass('active');
		$(this).addClass('active');
	});




	$('[data-role="js-slide-toggle"]').on('click', function(event) {
		event.preventDefault();
		$('#form-search-two').slideToggle(300);
		$('#search-two').toggleClass('__is-expanded');
	});




	// nav menu
	$('[data-role="button-open-menu"]').on('click', function(event) {
		$('body').toggleClass('__opened-nav-menu');
		$('html').toggleClass('__no-scroll');
	});




	// search popup icons
	$('.search.popup .list-block .jq-checkbox').each(function() {
		var $siblingLabel = $(this).siblings('label');
		$(this).insertAfter($siblingLabel);
	});




	// link inside link
	$('[data-role="redirect-link"]').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();

		var link = $(this).data('url');
		
		window.location.href=link;
	});

	$('[data-role="redirect-link"]').on('mouseenter', function(event) {
		var $parent = $(this).closest('a');
		$parent.addClass('__is-hovered');
	}).on('mouseleave', function(event) {
		var $parent = $(this).closest('a');
		$parent.removeClass('__is-hovered');
	});




	// tooltips
	$('.slidesjs-next.slidesjs-navigation').attr('title', '');
});