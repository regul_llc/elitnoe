ymaps.ready(init);

function init () {
myMap = new ymaps.Map("map", {
center: [55.740387, 37.527531],
zoom: 15
});
myMap.controls
  .add('smallZoomControl', { left: 5, top: 5 })

myPlacemark1 = new ymaps.Placemark([55.740387, 37.527531], {}, {
  iconImageHref: 'http://elitnoe.ru/images/symbols/cube.png',
  iconImageSize: [23, 24],
  iconImageOffset: [-12, -9]
});

myMap.geoObjects
  .add(myPlacemark1);

bigMap = new ymaps.Map("big_map", {
center: [55.740387, 37.527531],
zoom: 15
});

bigMap.controls
  .add('zoomControl', { left: 5, top: 5 })
  .add('typeSelector')
  .add('mapTools', { left: 35, top: 5 });

myPlacemark2 = new ymaps.Placemark([55.740387, 37.527531], {}, {
  iconImageHref: 'http://elitnoe.ru/images/symbols/cube.png',
  iconImageSize: [23, 24],
  iconImageOffset: [-12, -9]
});

bigMap.geoObjects
  .add(myPlacemark2);
}
