$(document).ready(function(){
  if ($.fn.mask) {
    $(".js-mask").mask("999 999 999");
  }

  if ($.fn.bxSlider) {
    $('.bxslider').bxSlider({
      auto: true,
      pause: 5000,
      pagerCustom: '#bx-pager'
    });
  }

  //Изменяем стиль ссылок
  $('.js-link').on('click', function (event) {
    event.preventDefault();

    $(this).toggleClass('__style-2');
  })

  // Прокрутка мелких картинок под слайдером
  var pagerContainer = $('.pager-container');
  if (pagerContainer.length) {
    var thumbs = pagerContainer.find('a');
    var thumbsWidth = thumbs.width();
    var thumbsNumber = thumbs.length;
    var pagerContainerWidth = thumbsNumber * thumbsWidth;
    pagerContainer.width(pagerContainerWidth);
  }

  var counter = 0;
  var countVisible = 11;
  var countMax = thumbsNumber - countVisible;
  var countMin = 0;

  // Пряем стрелки если картинок немного
  if ( thumbsNumber <= countVisible) {
    $('.icon-pagination').hide();
  }

  $('.icon-right-arrow').on('click', function () {
    if (counter < countMax) {
      counter++;
    }
    pagerContainer.animate({
      left: -thumbsWidth * counter + 'px'
    }, 300);
  });

  $('.icon-left-arrow').on('click', function () {
    if (counter > countMin) {
      counter--;
    }
    pagerContainer.animate({
      left: -thumbsWidth * counter + 'px'
    }, 300);
  });





  $('#show-agent-phone').on('click', function (event) {
    event.preventDefault();

    $(this).closest('.agent-contacts-small').toggleClass('__is-open');
    $(this).closest('.agent-contacts-small').siblings('.column.right').toggleClass('__is-positioned');

    $('#agent-phone').slideToggle();
    
    var $link = $(this);
    var text = $link.text();
    if (text === 'Скрыть контакты агента') {
      $link.text('Показать контакты агента');
    } else {
      $link.text('Скрыть контакты агента');
    }
  });

  $('.link-show-agent').on('click', function (event) {
    event.preventDefault();
    
    $(this).parent().siblings('.agent-contacts').slideToggle();
  });
  
  $('.container-404-main').height( $(document).height() );
  var strDefaultMaxDate = "15/7/2014"; //сюда втыкаем дефолтную максимальную дату.
  if ($.jdPicker) {
    $('#dpFrom').jdPicker({ date_max:strDefaultMaxDate });
    $('#dpTo').jdPicker({ date_max:strDefaultMaxDate });
    
    $('#dpFrom').on('onSelDate', function (event) {
      var dateFrom = $(this).val();
      $('#dpTo').jdPicker({ method: "destroy" });
      $('#dpTo').jdPicker({date_min: dateFrom, date_max:strDefaultMaxDate});
      console.log (dateFrom);
    });
    $('#dpTo').on('onSelDate', function (event) {
      var dateTo = $(this).val();
      $('#dpFrom').jdPicker({ method: "destroy" });
      $('#dpFrom').jdPicker({ date_max: dateTo });
    });
  }

  // Sticky map
  var mapFull = $('#huge_map');
  var mapFullOffsetTop;
  if (mapFull.length) {
    mapFullOffsetTop = mapFull.offset().top;
  }

  $(window).on('scroll' , function() {
    var windowScrollTop = $(window).scrollTop();
    if (mapFullOffsetTop && mapFullOffsetTop - windowScrollTop < -200) {
      mapFull.addClass('__is-sticky');
      // mapFull.stop().animate({height: '200px'});
    } else {
      $('#show-map').slideUp(function () {
        mapFull.slideDown();
      }); 
      mapFull.removeClass('__is-sticky');
      // mapFull.stop().animate({height: '403px'});
    }
  });

  $('#hide-map').on('click', function (event) {
    event.preventDefault();
    
    mapFull.slideUp(function () {
      $('#show-map').slideDown();
    });
  });

  $('#show-map').on('click', function () {
    $('#show-map').slideUp(function () {
      mapFull.slideDown();
    });        
  });



  $('#check-all').on('change', function () {
    var checkboxes = $('.search-results').find(':checkbox');
    checkboxes.prop('checked', this.checked);
  });
  


  $( document ).tooltip({
    position: {
      my: "right top",
      at: "right+10 bottom+10"
    }
  });

  $( '.tooltip-style-2' ).tooltip({
    position: {
      my: "right bottom",
      at: "right+20 top-10"
    },
    tooltipClass: 'tooltip-style-2'
  });


  $('.result__bottom .link-2').on('click', function (event) {
    event.preventDefault();

    var $this = $(this);

    var hiddenBlock = $this.parents('.search-results__result').find('.result__more-info');

    if ( hiddenBlock.is(':visible') ) {
      $this.text('Описание')
    } else {
      $this.text('Скрыть описание');
    }

    hiddenBlock.slideToggle();
  });



  $('.result .question').siblings().addClass('wrong-position');


  // Показываем ссылки разделов при наведении на имя пользователя в хедере
  $('#user-link').on('mouseenter', function () {
    $(this).parents('.user').addClass('__is-active');
  });
  $('.user').on('mouseleave', function () {
    $(this).removeClass('__is-active');
  });

  /* Разбитие журнала на колонки */
  var sections = jQuery('.journal .article-prev');
  if (sections.length > 0) {
    jQuery('.journal .article-main').after('<div class="column left"></div><div class="column center"></div><div class="column right"></div>');
    for (var i = 0; i < sections.length; i++) {
      // 1st column
      if (i % 3 == 0) {
        jQuery('.journal .column.left').append(sections[i]);

      // 2nd column
      } else if ((i - 1) % 3 == 0) {
        jQuery('.journal .column.center').append(sections[i]);

      // 3rd column
      } else if ((i - 2) % 3 == 0) {
        jQuery('.journal .column.right').append(sections[i]);
      }
    }
  }
  /**/

  var isUIDisabled = false;
  $('#select-two select').on('change', function() {
    if (!isUIDisabled && $(this).find(':selected').text().indexOf('Жилые комплексы') != -1) {
      setUIDisabled();

      isUIDisabled = true;
      return;
    }
    if (isUIDisabled) {
      setUIEnabled();

      isUIDisabled = false;        
    }
  });
  function setUIDisabled() {
    $('#radio4').button({disabled: true});
    $('#select-two3 select').attr('disabled', 'disabled').trigger('refresh');
    $('#cbPenthouses, #cbViews, #cbMoney').attr('disabled', 'disabled');
  }
  function setUIEnabled() {
    // $('#radio4').button({disabled: false});
    $('#select-two3 select').removeAttr('disabled').trigger('refresh');
    $('#cbPenthouses, #cbViews, #cbMoney').removeAttr('disabled');
  }

  $('#cbAgent').on('change', function() {
    if ($(this).is(':checked')) {
      $('#forAgents').slideDown();
    } else {
      $('#forAgents').slideUp();
    }
  });

  //Изменение цвета пунктов главного меню при наведении    
  $('#nav a').on('mouseover', function() {        
    $('#nav a').css('color', '#818181');
    if ($(this).attr('id') === 'magazin') {
      $(this).css('color', 'white');
    } else {
      $(this).css('color', '#3ca2a2');
    }
  }).on('mouseout', function() {        
    $('#nav a').css({
      'opacity': 1,
      'color': '#106767'
    });
    if ($(this).attr('id') === 'magazin' && $(this).parent().hasClass('current')) {
      $(this).css('color', 'white');
    }
  });

  $('.article .gallery-slider').each(function() {
    var slider = $(this);
    slider.find(".slides").carouFredSel({
      auto    : true,
      prev    : {
        button  : slider.find(".prev, .prev-block"),
        key     : "left"
      },
      next    : {
        button  : slider.find(".next, .next-block"),
        key     : "right"
      },
      pagination: slider.find(".pagination")
    });
  });

  $('.js-no-action').on('click', function(event) {
    event.preventDefault();
  });



  var formone = '<div class="search">'+
          '<form id="form-search">'+
            '<div id="radio">'+
              '<input type="radio" id="radio1" name="radio" checked="checked" /><label for="radio1" >Продажа</label>'+
              '<input type="radio" id="radio2" name="radio" /><label for="radio2" >Аренда</label>'+
            '</div>'+
            '<div id="select">'+
              '<select>'+
                '<option value="Квартира" selected="selected">Квартира</option>'+
                '<option value="Дом">Дом</option>'+
                '<option value="Комната">Комната</option>'+
              '</select>'+
            '</div>'+
            '<div id="priceot">'+
              '<label class="text" for="priceone">Цена от</label>'+
              '<input type="text" id="priceone" class="price" name="priceot"/>'+
              '<div class="clr"></div>'+
            '</div>'+
            '<div id="pricedo">'+
              '<label class="text" for="pricetwo">до</label>'+
              '<input type="text" id="pricetwo" class="price" name="pricedo"/>'+
              '<span class="text">руб.</span>'+
              '<div class="clr"></div>'+
            '</div>'+
            '<div id="submit">'+
              '<span class="text" id="objekt">243 объекта</span>'+
              '<input type="submit" class="submit-btn" value="Найти"/>'+
              '<div class="clr"></div>'+
            '</div>'+
            '<div class="clr"></div>'+
          '</form>'+
          '<a id="strelka"></a>'+
        '</div>';
    
   var formtwo =    '<form id="form-search">GGGGGGGGGGGGGGGGGGGGGGGGGGGG'+
            '<div id="radio">'+
              '<input type="radio" id="radio1" name="radio" checked="checked" /><label for="radio1" >Продажа</label>'+
              '<input type="radio" id="radio2" name="radio" /><label for="radio2" >Аренда</label>'+
            '</div>'+
            '<div id="select">'+
              '<select>'+
                '<option value="Квартира" selected="selected">Квартира</option>'+
                '<option value="Дом">Дом</option>'+
                '<option value="Комната">Комната</option>'+
              '</select>'+
            '</div>'+
            '<div id="priceot">'+
              '<label class="text" for="priceone">Цена от</label>'+
              '<input type="text" id="priceone" class="price" name="priceot"/>'+
              '<div class="clr"></div>'+
            '</div>'+
            '<div id="pricedo">'+
              '<label class="text" for="pricetwo">до</label>'+
              '<input type="text" id="pricetwo" class="price" name="pricedo"/>'+
              '<span class="text">руб.</span>'+
              '<div class="clr"></div>'+
            '</div>'+
            '<div id="submit">'+
              '<span class="text" id="objekt">243 объекта</span>'+
              '<input type="submit" class="submit-btn" value="Найти"/>'+
              '<div class="clr"></div>'+
            '</div>'+
            '<div class="clr"></div>'+
          '</form>';
   
   $('.strelka').click(function(){
    $("#search").slideUp("slow");
    $("#search-two").slideDown("slow");
   });
   
   $('.strelka-two').click(function(){
     $("#search-two").slideUp("slow");
     $("#search").slideDown("slow"); 
   });
  
   
   $(function() {
    $('#presentation_container').slidesjs({
    width: 1100,
    height: 621,
    // height: 550,
    play: {
      active: false,

      effect: "fade",

      interval: 3000,

      auto: false,

      swap: true,

      pauseOnHover: false,

      restartDelay: 2500            
      
    },
    navigation: {
      effect: "fade"
    },
    pagination: {
      effect: "fade"
    },
    effect: {
      fade: {
      speed: 1000
      }
    },
    callback: {
      loaded: function(number) {
      /* Выравнивание пагинатора в главном слайдере */
      var paginator = $('.slidesjs-pagination');
      paginator.css('margin-left', -paginator.width() / 2 + 'px');
      }
    }
    });
  });
   
   // var initialHeight = $('#search-two').height();
   // var offsetHeight = 100;
   // $('.dynamic-link label:not(#choices label)').on('mouseover', function() {
   //      $('#search-two').stop().animate({height: (initialHeight + offsetHeight) + 'px'}, 200);
   //      $(this).siblings('ul').stop().slideToggle();
   // }).on('mouseout', function() {
   //      $(this).siblings('ul').stop().slideToggle();
   //      $('#search-two').stop().animate({height: initialHeight + 'px'}, 200);
   // });

  $('.search.popup .body-content').height( $('.search.popup').height() - 200 );

  if ($.jScrollPane) {
    $('.search.popup .js-scrollbar').jScrollPane({
      showArrows: true,
      verticalArrowPositions: 'split',
      horizontalArrowPositions: 'split'
    });
  }

  $('.search.popup .tabs div').on('click', function() {
    var indexNext = $('.search.popup .tabs div').index($(this));
    var indexCurrent = $('.search.popup .tabs .current').index();

    if (indexCurrent === indexNext) {
      return;
    }

    openTab(indexNext);
  });
  function openTab(index) {
    $('.search.popup .tabs div')
              .removeClass('current')
              .eq(index)
              .addClass('current');

    $('.search.popup .body .content')
                    .removeClass('current')
                    .eq(index)
                    .addClass('current');

    $('.search.popup .js-scrollbar').jScrollPane({
      showArrows: true,
      verticalArrowPositions: 'split',
      horizontalArrowPositions: 'split'
    });
  }






  $('#get-circle').on('click', function(e) {      
    $('.content.metro :checked').removeAttr('checked').trigger('refresh');
    $('.content.metro :checkbox.circle').prop('checked', true).trigger('refresh');

    clearMetroSelectionList();
    addMetroToSelectionList();
    animateSearchBG();
  });
  $('#get-in-circle').on('click', function(e) {
    $('.content.metro :checkbox').removeAttr('checked').trigger('refresh');
    $('.content.metro :checkbox.in-circle').prop('checked', true).trigger('refresh');

    clearMetroSelectionList();
    addMetroToSelectionList();
    animateSearchBG();
  });

  $('.search.popup .content :checkbox').on('change', function() {
    console.log($(this));
    if ( $(this).is(':checked') ) {
      checkMetroLabel($(this));
    } else {
      uncheckMetroLabel($(this));
    }
    animateSearchBG();
  });

  $('.dynamic-link ul, .dynamic-link li, .selection-list ul, .selection-list li').on('click', function(e) {
    e.stopPropagation();
  });

  function checkMetroLabel(checkbox) {
    var contentCurrent = $('.search.popup .body .content.current');
    var listInsidePopup = contentCurrent.find('.selection-list ul');
    var contentClassName = '';
    if (contentCurrent.hasClass('metro')) {
      contentClassName = 'metro';
    } else if (contentCurrent.hasClass('streets')) {
      contentClassName = 'streets';
    } else {
      contentClassName = 'buildings';
    }
    var listOutsidePopup = $('#line-search2 .' + contentClassName + ' ul');
    var label = checkbox.siblings('label').text();

    listInsidePopup.append('<li>' + label + ' <span class="icon"></span></li>')
    listOutsidePopup.append('<li>' + label + ' <span class="icon"></span></li>');

    listInsidePopup.find('.icon').on('click', removeMetro);
    listOutsidePopup.find('.icon').on('click', removeMetro);
  }

  function uncheckMetroLabel(checkbox) {
    var label = checkbox.siblings('label').text();
    var contentCurrent = $('.search.popup .body .content.current');
    var listInsidePopup = contentCurrent.find('.selection-list ul');
    var contentClassName = '';
    if (contentCurrent.hasClass('metro')) {
      contentClassName = 'metro';
    } else if (contentCurrent.hasClass('streets')) {
      contentClassName = 'streets';
    } else {
      contentClassName = 'buildings';
    }
    var listOutsidePopup = $('#line-search2 .' + contentClassName + ' ul');

    listInsidePopup.find('li:contains("' + label + '")').remove();
    listOutsidePopup.find('li:contains("' + label + '")').remove();
  }

  function clearMetroSelectionList() {
    var contentCurrent = $('.search.popup .body .content.current');
    var contentClassName = '';
    if (contentCurrent.hasClass('metro')) {
      contentClassName = 'metro';
    } else if (contentCurrent.hasClass('streets')) {
      contentClassName = 'streets';
    } else {
      contentClassName = 'buildings';
    }
    var listOutsidePopup = $('#line-search2 .' + contentClassName + ' ul');
    var listInsidePopup = contentCurrent.find('.selection-list ul');

    listInsidePopup.empty();
    listOutsidePopup.empty();
  }

  function addMetroToSelectionList() {
    var contentCurrent = $('.search.popup .body .content.current');
    var checkedLabels = contentCurrent.find('.jq-checkbox.checked');
    var listInsidePopup = contentCurrent.find('.selection-list ul');

    var contentClassName = '';
    if (contentCurrent.hasClass('metro')) {
      contentClassName = 'metro';
    } else if (contentCurrent.hasClass('streets')) {
      contentClassName = 'streets';
    } else {
      contentClassName = 'buildings';
    }
    var listOutsidePopup = $('#line-search2 .' + contentClassName + ' ul');

    checkedLabels.each(function() {
      var label = $(this).siblings('label').text();
      listInsidePopup.append('<li>' + label + ' <span class="icon"></span></li>');
      listOutsidePopup.append('<li>' + label + ' <span class="icon"></span></li>');
    });

    listInsidePopup.find('.icon').on('click', removeMetro);
    listOutsidePopup.find('.icon').on('click', removeMetro);
  }

  function addStreetToSelectionList(street) {
    var contentCurrent = $('.search.popup .body .content.current');
    var listInsidePopup = contentCurrent.find('.selection-list ul');
    var listOutsidePopup = $('#line-search2 .streets ul');

    listInsidePopup.append('<li>' + street + ' <span class="icon"></span></li>');
    listOutsidePopup.append('<li>' + street + ' <span class="icon"></span></li>');

    listInsidePopup.find('.icon').on('click', removeStreet);
    listOutsidePopup.find('.icon').on('click', removeStreet);
  }

  function removeMetro(e) {
    e.stopPropagation();

    var labelText = $(e.target).parent().text().trim();
    var contentCurrent = $('.search.popup .body .content.current');
    var label = contentCurrent.find('.jq-checkbox.checked + label:contains(' +  labelText + ')');
    var checkbox = label.siblings(':checkbox');

    checkbox.removeAttr('checked').trigger('refresh');

    uncheckMetroLabel(checkbox);
    animateSearchBG();
  }

  function removeStreet(e) {
    e.stopPropagation();

    var label = $(e.target).parent().text().trim();
    var contentCurrent = $('.search.popup .body .content.current');
    var listInsidePopup = contentCurrent.find('.selection-list ul');
    var listOutsidePopup = $('#line-search2 .streets ul');

    listInsidePopup.find('li:contains("' + label + '")').remove();
    listOutsidePopup.find('li:contains("' + label + '")').remove();

  }

  function animateBackground() {
    animateSearchBG();
    $('.popup:visible').stop().fadeOut();
    $('#bg-overlay').stop().fadeOut();
  }

  function animateSearchBG() {
    var height_1 = $('#raion ul').height();
    var height_2 = $('#street ul').height();
    var height_3 = $('#bulding ul').height();
    var offsetHeight = Math.max(height_1, height_2, height_3) - 53;
    offsetHeight = initialHeight + offsetHeight;
    offsetHeight = (offsetHeight < initialHeight) ? initialHeight : offsetHeight;
    $('#search-two').stop().animate({height: (offsetHeight + 'px') }, 200);
  }

  $('#metro-reset').on('click', function(e) {
    e.preventDefault();

    var contentCurrent = $('.search.popup .body .content.current');
    contentCurrent.find(':checked').removeAttr('checked').trigger('refresh');
    clearMetroSelectionList();
  });

  var initialHeight = $('#search-two').height();
  $('#metro-submit').on('click', function(e) {
    e.preventDefault();
    $('html').removeClass('__no-scroll');

    animateBackground();
  });

  $('#street-button').on('click', function() {
    var streetName = $('#add-street').val();
    addStreetToSelectionList(streetName);
    $('#add-street').val('');
  });

  $('#add-street').on('keypress', function (event) {
    if (event.keyCode == 13) {
      var streetName = $('#add-street').val();
      addStreetToSelectionList(streetName);
      $('#add-street').val('');
      return false; // prevent the button click from happening
    }
  });
  
   



  $('#bg-overlay').height($('body').height());
  $('#bg-overlay').hide();
  //Открытие попапов
  $('#map-link').on('click', function(event) {        
    $('#bg-overlay').height($('body').height())
            .stop().fadeIn();

    $('.map.popup').css('top', $(document).scrollTop() + 50 + 'px');
    $('.map.popup').stop().fadeIn();
  });
  $('.link-map').on('click', function(event) {
    event.preventDefault();
    $('#bg-overlay').height($('body').height())
            .stop().fadeIn();

    $('.map-small.popup').css('top', $(document).scrollTop() + 50 + 'px');
    $('.map-small.popup').stop().fadeIn();
  });

  var dynamicLinkClicked = null;
  $('.dynamic-link:not(#choices)').on('click', function(event) {
    event.preventDefault();

    $('html').addClass('__no-scroll');
    
    dynamicLinkClicked = $(this);

    if ($(this).hasClass('metro')) {
      openTab(0);
    } else if ($(this).hasClass('streets')) {
      openTab(1);
    } else {
      openTab(2);
    }

    $('#bg-overlay').height($('body').height())
            .stop().fadeIn();

    $('.search.popup').css('top', $(document).scrollTop() + 50 + 'px');
    $('.search.popup').stop().fadeIn();
    $('.search.popup .js-scrollbar').jScrollPane({
      showArrows: true,
      verticalArrowPositions: 'split',
      horizontalArrowPositions: 'split'
    });
  });

  $('.closePopup').on('click', function() {
    $('html').removeClass('__no-scroll');

    if ($('.search.popup').is(":visible")) {
      animateSearchBG();      
    }

    $(this).parent().stop().fadeOut(function() {
      $(this).removeClass('visible');
    });
    $('#bg-overlay').stop().fadeOut(function() {
      $(this).removeClass('visible');
    });
  });

  $('#bg-overlay').on('click', function() {
    if ($('body').hasClass('__opened-nav-menu')) {
      return;
    }

    if ($('.search.popup').is(":visible")) {
      animateSearchBG();
    }

    if ($('.popup').is(":visible")) {
      $('html').removeClass('__no-scroll');
    }

    $('.popup:visible').stop().fadeOut(function() {
      $(this).removeClass('visible');
    });
    $('#bg-overlay').stop().fadeOut(function() {
      $(this).removeClass('visible');
    });
  });
  
  $('#login .enter').on('click', function() {
    $('#bg-overlay').height($('body').height())
            .stop().fadeIn();
    $('.password.popup').stop().fadeIn();
  });



  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });
  $('.scrollup').click(function(e) {
    e.preventDefault();

    $("html, body").animate({
      scrollTop: 0
    }, 600);        
  });
  



  $('.link-change-search').on('click', function(event) {
    event.preventDefault();

    $("#search").slideUp("fast");
    $("#search-two").slideDown("fast");
    $("html, body").animate({
      scrollTop: 0
    }, 600);
  });


  $('.selection-list').height( $('.search.popup .body-content').height() - 42 );




  $('.question').on('click', function() {
    $(this).parents('result').css('z-index', 10);
    $(this).find('.tip').fadeIn();
  }).on('mouseleave', function() {
    $(this).find('.tip').fadeOut(function() {
      $(this).parents('result').css('z-index', 8);
    });   
  });



  $('.file-images .images-container:nth-child(3n+1)').css('margin-left', '0');
});